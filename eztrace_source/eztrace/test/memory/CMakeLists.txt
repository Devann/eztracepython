enable_testing()

if (EZTRACE_ENABLE_MEMORY)

  set(CMAKE_C_FLAGS "-pthread")
  set(LINK_OPTIONS  "-pthread")

  add_executable(memory memory.c)
  add_test (build_memory "${CMAKE_COMMAND}" --build "${CMAKE_BINARY_DIR}" --target memory)
  add_test (memory_tests bash "${CMAKE_CURRENT_SOURCE_DIR}/run.sh" DEPENDS build_memory)
  set(EZTRACE_LIBRARY_PATH "${EZTRACE_LIBRARY_PATH}:${CMAKE_BINARY_DIR}/src/modules/memory")


  # Get the list of tests, and set environment variables
  get_property(test_list DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY TESTS)
  set_property(TEST ${test_list}
    PROPERTY ENVIRONMENT
    "EZTRACE_LIBRARY_PATH=${EZTRACE_LIBRARY_PATH}"
    ${TEST_ENVIRONMENT}
  )
endif()

#!/bin/bash
CUR_PATH=$(dirname  $(realpath $0))
source "$CUR_PATH/../test_utils/test_utils.sh"

name="memory"
run_and_check_command "$EZTRACE_PATH" $EZTRACE_TEST_OPTION -t "memory" "./$name"

trace_filename="${name}_trace/eztrace_log.otf2"

trace_check_integrity "$trace_filename" || exit 1
trace_check_enter_leave_parity  "$trace_filename" 

trace_check_nb_enter "$trace_filename" "realloc" 10
trace_check_nb_leave "$trace_filename" "realloc" 10

trace_check_nb_enter "$trace_filename" "calloc" 10
trace_check_nb_leave "$trace_filename" "calloc" 10

trace_check_nb_enter "$trace_filename" "malloc" 30
trace_check_nb_leave "$trace_filename" "malloc" 30

echo PASS: $nb_pass, FAILED:$nb_failed, TOTAL: $nb_test

exit $nb_failed

#include <stdio.h>
#include <stdlib.h>

extern inline int baz(int a) {
  return a+1;
}

static int bar(int a) {
  return baz(a+1);
}

int foo(int a) {
  return bar(a+1);
}

int main(int argc, char**argv) {

  int sum = 0;

  for(int i=0; i<10; i++) {
    sum += foo(i);
  }

  printf("sum = %d\n", sum);

  return EXIT_SUCCESS;
}

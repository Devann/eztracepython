#!/bin/bash

# Load function print_info, print_warning, print_error and print_ok
source test_utils/test_utils.sh


available_options="
  pthread
  memory
  ompt
  openmp
  posixio
  mpi
  starpu
  cuda
  compiler_instrumentation
"


# module_generator performance pptrace static

function usage {
  >&2 echo "Available tests are : $available_options"
  >&2 echo '"test/run list" print all the available tests on standard output'
  >&2 echo '"test/run ALL" is a shortcut for "test/run $(test/run list)" and run all the tests'
}

if [[ "$@" == "" ]] ; then
  usage
  exit
fi

asked_options="$@"

if [[ "$asked_options" == "ALL" ]] ; then
  asked_options="$available_options"
fi

if [[ "$asked_options" == "list" ]] ; then
  echo "$available_options"
  exit 0
fi

invalid_options=""

for opt in $asked_options ; do
  found=false
  for avail in $available_options ; do
    if [[ $opt == $avail ]] ; then
      found=true
    fi
  done
  if [[ $found == false ]]; then
    invalid_options+=$opt
  fi
done

if [[ $invalid_options != "" ]] ; then
  print_error "Invalid options found : \"$invalid_options\", available_options are : \"$available_options\""
  exit 1
fi

success=0
for opt in $asked_options ; do
  print_info $opt
  (cd $opt ; ./run.sh)
  if [[ $? == 0 ]] ; then
    print_ok "Test $opt succeded"
  else
    print_error "Test $opt failed : $res"
    success=1
  fi
done

exit $success

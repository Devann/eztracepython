# Existing plugins

EZTrace ships several plugins for the main parallel programming libraries.

### MPI

The MPI plugin traces MPI communications (including non-blocking
collectives), and supports MPI communicators. It supports the C and
Fortran interfaces.

It does not support (yet) MPI-IO primitives.

### OpenMP

EZTrace ships two plugins for OpenMP:
- `ompt` uses the OMPT interface to trace OpenMP parallel regions, loops, task, etc.

- `openmp` intercepts calls to the GNU OpenMP runtime system. It can
  only trace parallel region with GNU OpenMP. You can also instrument
  your OpenMP application with Opari to collect more events (including
  loops, tasks, ...), and to make it work with any OpenMP implementation

### Pthread

EZTrace traces pthread creation/destruction. You can also use the
`pthread` plugin to trace pthread synchronization functions
(eg. `pthread_mutex_*`, `pthread_cond_*`.

### PosixIO

EZTrace `posixio` plugin traces calls to PosixIO functions (`open`,
`read`, `write`, `dup`, `fread`, `fwrite`, ...

### Memory

EZTrace `memory` plugin traces calls to dynamic memory allocation functions (`malloc`,
`realloc`, `calloc`, `free`,  ...


### IOTracer

EZTrace `iotracer` plugin uses IOTracer to trace IO events that happen
within the Linux kernel. See [EZIOTracer webpage](https://gitlab.com/idiom1/eziotrace)
for instructions.


### CUDA

EZTrace `cuda` plugin traces CUDA events (memory copy, kernel
invocation, etc.) By default, the plugin captures events located on
the GPU (using the CUPTI interface), as well as calls to the CUDA
runtime API happening on the CPU.

#### Available options

Several options are availble for the `cuda` module. They can be specified using environment variables:

- `EZTRACE_CUDA_DRIVER`: enable tracing of the CUDA driver API, set 
- `EZTRACE_CUDA_BUFFER_SIZE=x`: set the size of the CUDA event buffer (default value: `2097152` bytes)


### NETCDF

EZTrace `netcdf` plugin traces calls to the [NetCDF](https://www.unidata.ucar.edu/software/netcdf/) library.

### PNETCDF

EZTrace `pnetcdf` plugin traces calls to the [PNetCDF](https://parallel-netcdf.github.io/) library.

### StarPU

EZTrace `starpu` plugin traces [StarPU](https://starpu.gitlabpages.inria.fr/)
events (task creation, task execution, etc.)

### Compiler_instrumentation

Module for tracing compiler-instrumented functions.

### PAPI (currently not supported)

EZTrace `papi` plugin collect harware counters using the PAPI library.
This plugin is currently not supported (we need to
port it to use the OTF2 trace format)


# Generating custom plugins

** NOT IMPLEMENTED IN EZTRACE-2.0 yet ! **

You can generate one plugin and instrument the functions you want to.
In order to generate your plugin, you need to create a file containing:

* The name of the library you want to trace (libNAME.so)
* A brief description of the library (optional)
* An ID to identify the module (0? is reserved for eztrace internal use. Thus,
  you can use any between 10 and ff)
* The prototype of the functions you want to instrument

Basically, the file should look like that:

BEGIN_MODULE
NAME example_lib
DESC "module for the example library"
ID 99
int example_do_event(int n)
double example_function1(double* array, int array_size)
END_MODULE

Now use eztrace_create_plugin to generate the plugin source code:

$ eztrace_create_plugin example.tpl
New Module
Module name: 'example_lib'
Module description: '"module for the example library"'
Module id: '99'
        emulate record_state for 'example_do_event'
Function 'example_do_event' done
        emulate record_state for 'example_function1'
Function 'example_function1' done
End of Module example_lib

The source code is generated in the output directory. Just type:

$ make

Now set the EZTRACE_LIBRARY_PATH to the appropriate directory and you are good
to go.

You can also specify (in the example.tpl file) the way a function is depicted in


the output trace. For instance:

int submit_job(int* array, int array_size)
BEGIN
 ADD_VAR("job counter", 1)
END

Specifies that when the submit_job function is called, the output trace should
increment the "job counter" variable. You can now track the value of a variable!

The test/module_generator directory contains several scripts that demonstrate
the various commands available.

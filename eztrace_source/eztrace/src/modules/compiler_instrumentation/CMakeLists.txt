set(CMAKE_C_COMPILER ${MPICC})

add_library(eztrace-compiler_instrumentation SHARED
  compiler_instrumentation.c
)

target_link_libraries(eztrace-compiler_instrumentation
PUBLIC
    eztrace-core
    eztrace-lib
    eztrace-instrumentation
)

target_include_directories(eztrace-compiler_instrumentation
  PUBLIC
    ${compiler_instrumentation_INCLUDE_DIRS}
  PRIVATE
    ${CMAKE_SOURCE_DIR}/src/core/include/eztrace-core/
)

#---------------------------------------------

install(TARGETS eztrace-compiler_instrumentation
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)

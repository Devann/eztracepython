#!/usr/bin/env python
# encoding: utf-8
import sys
import importlib.util

# Verify args
if len(sys.argv) > 1:
    python_file = sys.argv[1]
    sys.argv[1:] = sys.argv[2:]
else:
    print("<Usage> tracing.py python_file arg1 arg2 ...")
    exit()

# Load the eztracepython module
eztpy_module_name = "eztracepython"
libdir = "@CMAKE_INSTALL_PREFIX@/@CMAKE_INSTALL_LIBDIR@/libeztrace-python-perf.so"
eztpy_spec = importlib.util.spec_from_file_location(eztpy_module_name, libdir)
eztpy_module = importlib.util.module_from_spec(eztpy_spec)
sys.modules[eztpy_module_name] = eztpy_module

# Get filters and pass module name
source_module_name = f"eztracepython_{python_file.split('/')[-1].split('.')[0]}"
source_filename = python_file.split("/")[-1]
eztpy_module.register_filters(source_module_name, source_filename)
eztpy_module.print_python_version()

# Import tested script as a module and execute it
test_spec = importlib.util.spec_from_file_location(
    source_module_name, python_file)
test_module = importlib.util.module_from_spec(test_spec)
eztpy_module.set_tracer()
test_spec.loader.exec_module(test_module)

# Disable tracing and clean C structures
eztpy_module.unset_tracer()
eztpy_module.cleanup()

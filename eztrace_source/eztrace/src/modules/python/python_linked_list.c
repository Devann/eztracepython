/* -*- c-file-style: "GNU" -*- */
/*
 */

#ifndef _REENTRANT
#define _REENTRANT
#endif

#include <errno.h>
#include <eztrace-core/eztrace_config.h>
#include <eztrace-instrumentation/pptrace.h>
#include <eztrace-lib/eztrace.h>
#include <eztrace-lib/eztrace_module.h>
#include <otf2/OTF2_AttributeList.h>
#include <regex.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

static volatile int _python_initialized = 0;

#define CURRENT_MODULE python
DECLARE_CURRENT_MODULE;

#define NAME_SIZE 100
#define LINE_SIZE 1000

struct filters {
  int function_count;
  char** functions_filter;
  int module_count;
  char** modules_filter;
  int source_filter;
  int caller_filter;
  char* source_module_name;
};

struct timers {
  double search_function_timer;
  double register_filters_timer;
  double free_list_timer;
  double eztrace_python_enter_timer;
  double eztrace_python_leave_timer;
};

struct time_calc {
  struct timespec start;
  struct timespec end;
};

struct event_tuple {
  char* event_type;
  char* function_name;
};

// Function linked list
struct Function {
  int id;
  char name[NAME_SIZE];
  struct Function* next;
};

// Variables setup
struct Function* functions = NULL;
struct filters my_filters;
struct timers my_timers = {0, 0, 0, 0, 0};
struct time_calc timer_calc_0;
struct time_calc timer_calc_1;
int timer_enabled = 0;
int nb_functions = 0;

// Start timer
void start_timer(struct time_calc* timer) {
  if (timer_enabled) {
    clock_gettime(CLOCK_MONOTONIC, &(timer->start));
  }
}

// End timer
void end_timer(struct time_calc* timer, double* acc_timer) {
  if (timer_enabled) {
    clock_gettime(CLOCK_MONOTONIC, &(timer->end));
    long seconds = timer->end.tv_sec - timer->start.tv_sec;
    long nanoseconds = timer->end.tv_nsec - timer->start.tv_nsec;
    *(acc_timer) += seconds + nanoseconds * 1e-9;
  }
}

// Linked list
void add_function(const char* name) {
  struct Function* new_function = (struct Function*)malloc(sizeof(struct Function));
  if (new_function == NULL) {
    perror("Failed to allocate memory for a new function");
    exit(EXIT_FAILURE);
  }
  strncpy(new_function->name, name, NAME_SIZE);
  new_function->id = ezt_otf2_register_function(name);
  new_function->next = functions;
  functions = new_function;
  nb_functions++;
}

// Search for a function by name in the linked list
int search_function(const char* name) {
  start_timer(&timer_calc_0);
  struct Function* current = functions;
  while (current != NULL) {
    if (strcmp(current->name, name) == 0) {
      // Found a match
      end_timer(&timer_calc_0, &(my_timers.search_function_timer));
      return current->id;
    }
    current = current->next;
  }
  // Name not found in the list, register
  add_function(name);
  end_timer(&timer_calc_0, &(my_timers.search_function_timer));
  return functions->id;
}

void free_list() {
  struct Function* current = functions;
  while (current != NULL) {
    struct Function* next = current->next;
    free(current);
    current = next;
  }
  nb_functions = 0;
}

char** read_file_content(char* filename, int* word_count) {
  FILE* file = fopen(filename, "r");
  if (file == NULL) {
    perror("Error opening file");
    exit(EXIT_FAILURE);
  }

  int array_size = 10;
  char** words = (char**)malloc(array_size * sizeof(char*));
  if (words == NULL) {
    perror("Failed to allocate memory for words");
    exit(EXIT_FAILURE);
  }
  *word_count = 0;
  char line[LINE_SIZE];

  while (fgets(line, sizeof(line), file) != NULL) {
    char* token = strtok(line, " \t\n");

    while (token != NULL) {
      if (*word_count >= array_size) {
        array_size *= 2;
        char** temp = (char**)realloc(words, array_size * sizeof(char*));
        if (temp == NULL) {
          perror("Failed to reallocate memory for words");
          for (int i = 0; i < *word_count; i++) {
            free(words[i]);
          }
          free(words);
          exit(EXIT_FAILURE);
        }
        words = temp;
      }
      words[*word_count] = strndup(token, NAME_SIZE);
      (*word_count)++;
      token = strtok(NULL, " \t\n");
    }
  }
  fclose(file);
  return words;
}

char** get_filter_from_env(char* env_name, int* word_count) {
  char* env_value = getenv(env_name);
  if (env_value) {
    // If the env variable is a filename
    const char* pattern = "\\b\\S*\\.txt\\b";
    regex_t regex;
    if (regcomp(&regex, pattern, REG_EXTENDED) != 0) {
      perror("Failed to compile regex pattern\n");
      exit(EXIT_FAILURE);
    }
    regmatch_t match;
    if (regexec(&regex, env_value, 1, &match, 0) == 0) {
      char** content = read_file_content(env_value, word_count);
      return content;
    } else {
      // If the env variable is a string
      int array_size = 10;
      char** content = (char**)malloc(array_size * sizeof(char*));
      if (content == NULL) {
        perror("Failed to allocate mempry for words");
        exit(EXIT_FAILURE);
      }
      *word_count = 0;
      char* token = strtok(env_value, " ");
      while (token != NULL) {
        if (*word_count >= array_size) {
          array_size *= 2;
          char** temp = realloc(content, array_size * sizeof(char*));
          if (temp == NULL) {
            perror("Failed to reallocate memory for words");
            for (int i = 0; i < *word_count; i++) {
              free(content[i]);
            }
            free(content);
            exit(EXIT_FAILURE);
          }
          content = temp;
        }
        content[*word_count] = strndup(token, NAME_SIZE);
        token = strtok(NULL, " ");
        word_count++;
      }
      return content;
    }
  } else {
    return NULL;
  }
}

void register_filters(char* source_module_name) {
  start_timer(&timer_calc_0);

  int function_list_size = -1;
  char** function_list = get_filter_from_env("EZTPY_FUNCTION_FILTER", &function_list_size);
  int module_list_size = -1;
  char** module_list = get_filter_from_env("EZTPY_MODULE_FILTER", &module_list_size);
  char* source_filter_str = getenv("EZTPY_SOURCE_FILTER");
  int source_filter = -1;
  if (source_filter_str) {
    source_filter = atoi(source_filter_str);
  }
  char* caller_filter_str = getenv("EZTPY_CALLER_FILTER");
  int caller_filter = -1;
  if (caller_filter_str) {
    caller_filter = atoi(caller_filter_str);
  }

  my_filters.function_count = function_list_size;
  my_filters.functions_filter = function_list;
  my_filters.module_count = module_list_size;
  my_filters.modules_filter = module_list;
  my_filters.source_filter = source_filter;
  my_filters.caller_filter = caller_filter;
  my_filters.source_module_name = source_module_name;

  end_timer(&timer_calc_0, &(my_timers.register_filters_timer));
}

void unregister_filters() {
  for (int i = 0; i < my_filters.function_count; i++) {
    free(my_filters.functions_filter[i]);
  }
  for (int i = 0; i < my_filters.module_count; i++) {
    free(my_filters.modules_filter[i]);
  }
  free(my_filters.functions_filter);
  free(my_filters.modules_filter);
}

void print_filters() {
  printf("%s\n", "Function Filter :");
  if (my_filters.function_count == -1) {
    printf("%s", "No function filter set up.");
  } else {
    for (int i = 0; i < my_filters.function_count; i++) {
      puts(my_filters.functions_filter[i]);
    }
  }
  printf("%s\n", "Module Filter :");
  if (my_filters.module_count == -1) {
    printf("%s\n", "No module filter set up.");
  } else {
    for (int i = 0; i < my_filters.module_count; i++) {
      puts(my_filters.modules_filter[i]);
    }
  }
  if (my_filters.source_filter == -1) {
    printf("%s\n", "No source filter set up.");
  } else {
    printf("Source filter = %d", my_filters.source_filter);
  }
  if (my_filters.caller_filter == -1) {
    printf("%s\n", "No caller filter set up.");
  } else {
    printf("Caller filter = %d", my_filters.caller_filter);
  }
}

void print_timers() {
  printf("eztrace_python_enter: %f seconds\n", my_timers.eztrace_python_enter_timer);
  printf("eztrace_python_leave: %f seconds\n", my_timers.eztrace_python_leave_timer);
  printf("search_function: %f seconds\n", my_timers.search_function_timer);
  printf("register_filters: %f seconds\n", my_timers.register_filters_timer);
  printf("free_list: %f seconds\n", my_timers.free_list_timer);
}

int filter_function(char* func_name) {
  if (my_filters.function_count == -1) {
    return 0;
  }
  for (int i = 0; i < my_filters.function_count; i++) {
    if (strcmp(func_name, my_filters.functions_filter[i]) == 0) {
      return 0;
    }
  }
  return 1;
}

int filter_module(char* module_name) {
  if (my_filters.module_count == -1) {
    return 0;
  }
  if (module_name == NULL) {
    return 0;
  }
  for (int i = 0; i < my_filters.module_count; i++) {
    if (strcmp(module_name, my_filters.modules_filter[i]) == 0) {
      return 0;
    }
  }
  return 1;
}

void eztrace_python_enter(char* function_name) {
  int function_id = search_function(function_name);
  start_timer(&timer_calc_1);
  EZT_OTF2_EvtWriter_Enter(evt_writer,
                           NULL,
                           ezt_get_timestamp(),
                           function_id);
  end_timer(&timer_calc_1, &(my_timers.eztrace_python_enter_timer));
}

void eztrace_python_leave(char* function_name) {
  int function_id = search_function(function_name);
  start_timer(&timer_calc_1);
  EZT_OTF2_EvtWriter_Leave(evt_writer,
                           NULL,
                           ezt_get_timestamp(),
                           function_id);
  end_timer(&timer_calc_1, &(my_timers.eztrace_python_leave_timer));
}

int filtering_in_c(char* event, char* module_name, char* func_name) {

  // events filter
  if ((strcmp(event, "call") == 0) && (strcmp(event, "return") == 0)) {
    return 1;
  }

  // importlib filter
  if (module_name != NULL && (module_name, "importlib", strlen("importlib")) == 0) {
    return 1;
  }

  // write filter
  if (strcmp(func_name, "write") == 0) {
    return 1;
  }

  // source filter
  if (((my_filters.source_filter) == 1) && module_name != NULL && (strcmp(module_name, my_filters.source_module_name) != 0)) {
    return 1;
  }

  // function filter
  if (filter_function(func_name) == 1) {
    return 1;
  }

  // module filter
  if (filter_module(module_name) == 1) {
    return 1;
  }

  // Not filtered
  if (strcmp(event, "call") == 0) {
    eztrace_python_enter(func_name);
  } else if (strcmp(event, "return") == 0) {
    eztrace_python_leave(func_name);
  }
  return 0;
}

void process_batch(struct event_tuple* event_tuple_list, int event_tuple_list_size) {
  for (int i = 0; i < event_tuple_list_size; i++) {
    if (strcmp(event_tuple_list[i].event_type, "call") == 0) {
      eztrace_python_enter(event_tuple_list[i].function_name);
    } else if (strcmp(event_tuple_list[i].event_type, "return") == 0) {
      eztrace_python_leave(event_tuple_list[i].function_name);
    } else {
      perror("Event type not recognized");
      exit(EXIT_FAILURE);
    }
  }
}

PPTRACE_START_INTERCEPT_FUNCTIONS(python)
PPTRACE_END_INTERCEPT_FUNCTIONS(python)

static void init_python() {
  INSTRUMENT_FUNCTIONS(python);

  if (eztrace_autostart_enabled())
    eztrace_start();

  _python_initialized = 1;
}

static void finalize_python() {
  _python_initialized = 0;

  eztrace_stop();
}

static void _python_init(void) __attribute__((constructor));
static void _python_init(void) {
  char* timer_str = getenv("EZTRACEPYTHON_TESTING");
  if (timer_str) {
    if (strcmp(timer_str, "FILTERING_PYTHON_TIMERS") == 0 || strcmp(timer_str, "FILTERING_C_TIMERS") == 0 || strcmp(timer_str, "TRACING_PYTHON_LL") == 0) {
      timer_enabled = 1;
    }
  }
  eztrace_log(dbg_lvl_debug, "eztrace_python constructor starts\n");
  EZT_REGISTER_MODULE(python, "Module for python functions",
                      init_python, finalize_python);
  eztrace_log(dbg_lvl_debug, "eztrace_pythonconstructor ends\n");
}

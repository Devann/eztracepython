/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#include <eztrace-lib/eztrace_module.h>
#include <eztrace-lib/eztrace.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int verbose_mode = 0;
/* This program list the available modules
 */
static void usage(int argc __attribute__((unused)), char** argv) {
  fprintf(stderr, "Usage : %s [OPTION]\n", argv[0]);
  fprintf(stderr, "\t--version      Display the version number\n");
  fprintf(stderr, "\t-h         Display this help and exit\n");
}

void print_version() {
  printf( "eztrace_avail: version %s\n", EZTRACE_VERSION );
  exit( EXIT_SUCCESS );
}

static void parse_args(int argc, char** argv) {
  int i;
  for (i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-h") == 0) {
      usage(argc, argv);
      exit(-1);
    } else if (strcmp(argv[i], "-v") == 0) {
      printf("Verbose mode turned on\n");
      verbose_mode = 1;
    } else if (strcmp(argv[i], "--version") == 0) {
      print_version();
    }
  }
}

int main(int argc, char** argv) {
  /* clear the EZTRACE_TRACE environment variable so that all the available modules are listed */
  unsetenv("EZTRACE_TRACE");

  setenv("EZTRACE_DONT_TRACE", "1", 1);

  /* parse the arguments passed to this program */
  parse_args(argc, argv);

  eztrace_load_all_modules(verbose_mode);

  printf("Available modules:\n");
  eztrace_print_module_list();

  return 0;
}

import numpy as np
import time

# Generate synthetic data
x = np.linspace(-np.pi, np.pi, 2000)
y = np.sin(x)

# Define the model parameters
w = np.array([0.0, 0.0, 0.0, 0.0], dtype=np.float32)

# Learning rate
learning_rate = 1e-6

# Number of iterations
num_iterations = 2000

# Record start time
t1 = time.time()

# Perform gradient descent
for t in range(num_iterations):
    # Forward pass
    y_pred = np.dot(np.column_stack([x**i for i in range(4)]), w)

    # Compute the loss
    loss = np.sum((y_pred - y)**2)

    if t % 100 == 99:
        print(t, loss)

    # Backward pass (compute gradients)
    gradients = -2 * np.dot(np.transpose(np.column_stack([x**i for i in range(4)])), (y - y_pred))

    # Update parameters using gradient descent
    w -= learning_rate * gradients

# Record end time
t2 = time.time()

# Calculate and print the duration
duration = t2 - t1
print(f"-------------------------------------------\n")
print(f"-------------------------------------------\n")
print(f"Execution Time: {duration:.4f} seconds\n")
# Print the result
print(f'Result: y = {w[0]} + {w[1]} x + {w[2]} x^2 + {w[3]} x^3')
print(f"-------------------------------------------\n")
print(f"-------------------------------------------\n")


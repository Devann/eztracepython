import pandas as pd
import random as rd


def create_dataframe():
    data = {'Name': ['Alice', 'Bob', 'Charlie', 'David'],
            'Age': [25, 30, 22, 35],
            'City': ['New York', 'San Francisco', 'Los Angeles', 'Chicago']}

    df = pd.DataFrame(data)
    return df


def display_dataframe(df):
    print("DataFrame:")
    print(df)


def filter_dataframe(df, city):
    filtered_df = df[df['City'] == city]
    return filtered_df


def add_column(df, column_name, values):
    df[column_name] = values
    return df


def calculate_mean_age(df):
    mean_age = df['Age'].mean()
    return mean_age


def add_random_line(df):
    new_row = [f'Person_{len(df)+1}', rd.randint(18, 50), rd.choice(
        ['New York', 'San Francisco', 'Los Angeles', 'Chicago']), rd.choice([True, False])]
    df.loc[len(df)] = new_row
    return df


def random_aging(df):
    df['Age'] += [rd.randint(2, 11) for _ in range(len(df))]
    return df


def random_deaging(df):
    df['Age'] -= [rd.randint(1, min(age, 10)) for age in df['Age']]
    df['Age'] = df['Age'].clip(lower=0)
    return df


# Create DataFrame
my_dataframe = create_dataframe()

# Display original DataFrame
display_dataframe(my_dataframe)

# Filter DataFrame based on a city
city_to_filter = 'New York'
for i in range(1000):
    filtered_dataframe = filter_dataframe(my_dataframe, city_to_filter)

# Display filtered DataFrame
print(f"\nFiltered DataFrame for {city_to_filter}:")
display_dataframe(filtered_dataframe)

# Add a new column to DataFrame
new_column_values = [True, False, True, False]
my_dataframe = add_column(my_dataframe, 'IsStudent', new_column_values)

# Add Randomized lines
for i in range(200):
    my_dataframe = add_random_line(my_dataframe)

# Display DataFrame with the new column
print("\nDataFrame with added column:")
display_dataframe(my_dataframe)

# Calculate mean age
for i in range(200):
    my_dataframe = random_aging(my_dataframe)
    my_dataframe = random_deaging(my_dataframe)

display_dataframe(my_dataframe)
mean_age = calculate_mean_age(my_dataframe)
print(f"\nMean Age: {mean_age}")

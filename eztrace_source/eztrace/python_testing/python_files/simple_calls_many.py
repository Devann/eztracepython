import sys
NB_EXEC = 20000
NB_EXEC_INSIDE = 200
sum_a = 0
sum_b = 0
arg1 = 5
arg2 = 3


def b(arg):
    global sum_b
    sum_b += int(arg)
    return 1


def a(arg1, arg2):
    global sum_a
    sum_a += int(arg1)
    for i in range(NB_EXEC_INSIDE):
        b(arg2)
    return 2


for i in range(NB_EXEC):
    a(arg1, arg2)

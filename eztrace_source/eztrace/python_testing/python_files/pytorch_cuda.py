import torch
import torch.nn as nn
import torch.optim as optim

# Define a simple neural network


class SimpleNet(nn.Module):
    def __init__(self):
        super(SimpleNet, self).__init__()
        self.fc1 = nn.Linear(10, 5)
        self.fc2 = nn.Linear(5, 2)

    def forward(self, x):
        x = torch.relu(self.fc1(x))
        x = self.fc2(x)
        return x


if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    print("Cuda not available")
    raise SystemExit(-1)

model = SimpleNet()

model.to(device)

# Dummy input data
input_data = torch.randn(3, 10).to(device)

# Loss function and optimizer
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(model.parameters(), lr=0.01)

# Training
for epoch in range(100):
    # Forward pass
    outputs = model(input_data)

    # Compute loss
    targets = torch.LongTensor([0, 1, 0]).to(device)
    loss = criterion(outputs, targets)

    # Backward pass and optimization
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    # Print loss every 10 epochs
    if (epoch + 1) % 10 == 0:
        print(f'Epoch [{epoch+1}/100], Loss: {loss.item():.4f}')

#!/bin/bash

# VARIABLES
SCRIPT_NAME=$1

# FOLDER NAMES
ROOT_DIR="./../.."
PYTHON_DIR="$ROOT_DIR/python_files"
LOG_DIR="$ROOT_DIR/logs"
TIMES_TRACE_DIR="$LOG_DIR/times_trace"
OTF2_TRACE_DIR="$LOG_DIR/otf2_trace"
FILE="${TIMES_TRACE_DIR}/${EZTRACEPYTHON_TESTING}_${SCRIPT_NAME}.txt"
FILE_FUNCTIONS="$ROOT_DIR/scripts/tests/function_filter.txt"

# CREATE FUNCTION_FILTER.TXT
if [ $SCRIPT_NAME == "pytorch_nn" ]; then
    echo "linspace sin tensor unsqueeze print zero_grad backward parameters" > "$FILE_FUNCTIONS"
elif [ $SCRIPT_NAME == "simple_calls_many" ]; then
    echo "a" > "$FILE_FUNCTIONS"
elif [ $SCRIPT_NAME == "numpy_nn" ]; then
    echo "linspace sin array time dot sum transpose column_stack" > "$FILE_FUNCTIONS"
elif [ $SCRIPT_NAME == "pandas_example" ]; then
    export EZTPY_SOURCE_FILTER=1
    echo "" > "$FILE_FUNCTIONS"
else
    echo "a" > "$FILE_FUNCTIONS"
fi

# EXECUTE BENCHMARK
if [ "$EZTRACEPYTHON_TESTING" == "PYTHON" ]; then
    if ! { time python3 ${PYTHON_DIR}/${SCRIPT_NAME}.py ; } >/dev/null 2> $FILE; then
        echo "[ERROR] Execution ${SCRIPT_NAME}.py with EZTRACEPYTHON_TESTING = ${EZTRACEPYTHON_TESTING}"
        exit 1
    fi
elif [ "$EZTRACEPYTHON_TESTING" == "FULL" ]; then
    unset EZTPY_FUNCTION_FILTER
    if ! { time ./../../../install/bin/eztrace.preload -t python -o ${OTF2_TRACE_DIR}/${EZTRACEPYTHON_TESTING}_${SCRIPT_NAME} ${PYTHON_DIR}/${SCRIPT_NAME}.py; } >/dev/null 2> "$FILE"; then
        echo "[ERROR] Execution ${SCRIPT_NAME}.py with EZTRACEPYTHON_TESTING = ${EZTRACEPYTHON_TESTING}"
        exit 1
    fi
elif [ "$EZTRACEPYTHON_TESTING" == "FULL_IN_C" ]; then
    export EZTRACEPYTHON_TESTING="TRACING_FULL_C"
    unset EZTPY_FUNCTION_FILTER
    if ! { time ./../../../install/bin/eztrace.preload -t python -o ${OTF2_TRACE_DIR}/${EZTRACEPYTHON_TESTING}_${SCRIPT_NAME} ${PYTHON_DIR}/${SCRIPT_NAME}.py; } >/dev/null 2> "$FILE"; then
        echo "[ERROR] Execution ${SCRIPT_NAME}.py with EZTRACEPYTHON_TESTING = ${EZTRACEPYTHON_TESTING}"
        exit 1
    fi
elif [ "$EZTRACEPYTHON_TESTING" == "FULL+FILTER" ]; then
    export EZTPY_FUNCTION_FILTER="$FILE_FUNCTIONS"
    if ! { time ./../../../install/bin/eztrace.preload -t python -o ${OTF2_TRACE_DIR}/${EZTRACEPYTHON_TESTING}_${SCRIPT_NAME} ${PYTHON_DIR}/${SCRIPT_NAME}.py; } >/dev/null 2> "$FILE"; then
        echo "[ERROR] Execution ${SCRIPT_NAME}.py with EZTRACEPYTHON_TESTING = ${EZTRACEPYTHON_TESTING}"
        exit 1
    fi
elif [ "$EZTRACEPYTHON_TESTING" == "FULL_IN_C+FILTER" ]; then
    export EZTRACEPYTHON_TESTING="TRACING_FULL_C"
    export EZTPY_FUNCTION_FILTER="$FILE_FUNCTIONS"
    if ! { time ./../../../install/bin/eztrace.preload -t python -o ${OTF2_TRACE_DIR}/${EZTRACEPYTHON_TESTING}_${SCRIPT_NAME} ${PYTHON_DIR}/${SCRIPT_NAME}.py; } >/dev/null 2> "$FILE"; then
        echo "[ERROR] Execution ${SCRIPT_NAME}.py with EZTRACEPYTHON_TESTING = ${EZTRACEPYTHON_TESTING}"
        exit 1
    fi
fi


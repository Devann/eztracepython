# MODULES
import os
import sys
import pandas as pd
import matplotlib.pyplot as plt

# ARGUMENTS
if len(sys.argv) == 3:
    script_name = sys.argv[1]
    data_file = sys.argv[2]
else:
    print("Usage: display_results.py <data_file> <script_name>")
    exit()

# VÉRIFICATION DE L'EXISTENCE DU FICHIER
if not os.path.exists(data_file):
    print(f"[ERROR] In {sys.argv[0]}, the following file does not exist : {data_file}")
    exit()

# VARIABLES
CAMPAIGN_RESULTS="../../results/campaign1"
TITLE = f'[CAMPAIGN 1] Repartition of the execution time of {script_name}'
xlabel = 'Nom du script'
ylabel = 'Temps (s)'

# FETCH DATA
df = pd.read_csv(data_file, sep='\s+', skiprows=2, names=['SCRIPT', 'RESULT'])
df_diff = df[['SCRIPT', 'RESULT']]
plt.figure(figsize=(10, 6))

# DISPLAY
colors = ['#D32F2F', '#388E3C', '#1976D2', '#FFA000']
LABELS = ['PYTHON', 'PYTHON + INSTRUMENTATION', 'FILTERING = PYTHON + INSTRUMENTATION + FILTERING_EVENT', 'FULL = PYTHON + INSTRUMENTATION + FILTERING_EVENT + REGISTERING']

for i in range(len(df['SCRIPT'])):
    plt.bar(df['SCRIPT'][i], df['RESULT'][i], color=colors[i], label=LABELS[i])
    plt.text(df['SCRIPT'][i], df['RESULT'][i] + 0.01 * max(df['RESULT']),
             f"{df['RESULT'][i]:.3f} s", ha='center', va='bottom')

plt.grid(axis='y', linestyle='--', alpha=0.3)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.title(TITLE, fontsize=16, fontweight='bold')
plt.ylim(0, max(df['RESULT']) + max(df['RESULT'])/2)
plt.legend(title="Scripts", loc='upper left')

# SAVE
output_file = f'{CAMPAIGN_RESULTS}/c1_result_{script_name}.png'
plt.savefig(output_file)
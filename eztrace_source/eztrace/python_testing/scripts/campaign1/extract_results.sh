#!/bin/bash

# VARIABLES
EZTRACEPYTHON_TESTING_LIST=("PYTHON" "INSTRUMENTATION" "FILTERING" "FULL")

# FOLDER NAMES
LOG_DIR="./../../logs"
TIMES_TRACE_DIR="$LOG_DIR/times_trace"
OTF2_TRACE_DIR="$LOG_DIR/otf2_trace"

# ARGUMENTS
if [ $# -ne 2 ]; then
    echo "Usage: $0 SCRIPT_NAME OUTPUT_FILE"
    exit 1
fi
SCRIPT_NAME=$1
OUTPUT_FILE=$2
if [ -e "$OUTPUT_FILE" ]; then
    rm "$OUTPUT_FILE"
fi

# EXTRACT RESULTS
convert_to_seconds() {
    local duration=$1
    local minutes=$(echo "$duration" | cut -dm -f1)
    local seconds=$(echo "$duration" | cut -dm -f2 | tr -d 's' | tr ',' '.')    
    local total_seconds=$(echo "$minutes * 60 + $seconds" | bc)
    if [ "${total_seconds:0:1}" = "." ]; then
        total_seconds="0$total_seconds"
    fi
    echo "$total_seconds"
}

# DISPLAY RESULTS TABLE
echo -e "\e[1;42m\e[1;33m Results for ${SCRIPT_NAME}.py \e[0m"
echo -e "\e[1;41m\e[1;33m Performance \e[0m"
echo "${SCRIPT_NAME}.py" >> "$OUTPUT_FILE"
printf "%-20s %-10s %-10s\n" "SCRIPT" "RESULT" "INCREASE"
echo -e "SCRIPT RESULT">> "$OUTPUT_FILE"
previous_value=0
percentage_increase=0
for EZTRACEPYTHON_TESTING in "${EZTRACEPYTHON_TESTING_LIST[@]}"; do
    file="${TIMES_TRACE_DIR}/${EZTRACEPYTHON_TESTING}_${SCRIPT_NAME}.txt"
    result=$(convert_to_seconds "$(grep "real" "$file" | awk '{print $2}')")
    if [ $(echo "$previous_value != 0" | bc) -eq 1 ]; then
        percentage_increase=$(echo "scale=2; (($result - $previous_value) / $previous_value) * 100" | bc)
    fi
    previous_value=$result
    printf "%-20s %-10s %-10s\n" "$EZTRACEPYTHON_TESTING" "${result}s" "${percentage_increase}%"
    echo "$EZTRACEPYTHON_TESTING $result" >> "$OUTPUT_FILE"
done

# COMPUTE THE ADDITIONAL COST PER EVENT
calculate_overhead() {
    echo -e "\e[1;41m\e[1;33m Overhead \e[0m"
    local file_vanilla="${TIMES_TRACE_DIR}/PYTHON_${SCRIPT_NAME}.txt"
    local file_script="${TIMES_TRACE_DIR}/FULL_${SCRIPT_NAME}.txt"

    local result_vanilla=$(convert_to_seconds "$(grep "real" "$file_vanilla" | awk '{print $2}')")
    local result_script=$(convert_to_seconds "$(grep "real" "$file_script" | awk '{print $2}')")
    local diff=$(echo $result_script - $result_vanilla | bc)

    local file_events="${OTF2_TRACE_DIR}/FULL_${SCRIPT_NAME}/eztrace_log.otf2"
    local number_events=$(otf2-print "$file_events" 2>&1 | tail -n +6 | wc -l)
    local overhead_per_event=$(echo "scale=7; $diff / $number_events" | bc)

    printf "%-30s %-9s\n" "Overhead for ${number_events} events" "${diff}s"
    printf "%-30s %-9s\n" "Overhead per event" "${overhead_per_event}s"
    echo ""
}

calculate_overhead
exit 0
#!/bin/bash

# VARIABLES
SCRIPT_NAME=$1

# FOLDER NAMES
ROOT_DIR="./../.."
PYTHON_DIR="$ROOT_DIR/python_files"
LOG_DIR="$ROOT_DIR/logs"
TIMES_TRACE_DIR="$LOG_DIR/times_trace"
OTF2_TRACE_DIR="$LOG_DIR/otf2_trace"
FILE="${TIMES_TRACE_DIR}/${EZTRACEPYTHON_TESTING}_${SCRIPT_NAME}.txt"

# EXECUTE BENCHMARK
if [ "$EZTRACEPYTHON_TESTING" == "PYTHON" ]; then
    if ! { time python3 ${PYTHON_DIR}/${SCRIPT_NAME}.py ; } >/dev/null 2> $FILE; then
        echo "[ERROR] Execution ${SCRIPT_NAME}.py with EZTRACEPYTHON_TESTING = ${EZTRACEPYTHON_TESTING}"
        exit 1
    fi
else
    if ! { time ./../../../install/bin/eztrace.preload -t python -o ${OTF2_TRACE_DIR}/${EZTRACEPYTHON_TESTING}_${SCRIPT_NAME} ${PYTHON_DIR}/${SCRIPT_NAME}.py; } >/dev/null 2> "$FILE"; then
        echo "[ERROR] Execution ${SCRIPT_NAME}.py with EZTRACEPYTHON_TESTING = ${EZTRACEPYTHON_TESTING}"
        exit 1
    fi
fi

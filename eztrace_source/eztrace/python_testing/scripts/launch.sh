# START THE CAMPAIGN
echo -e "\e[1;41m  \e[1;33m =============================================================== \e[0m "
echo -e "\e[1;41m  \e[1;33m ===                 STARTING CAMPAIGNS                      === \e[0m "
echo -e "\e[1;41m  \e[1;33m =============================================================== \e[0m "

cd "./campaign1"
./campaign.sh

cd "../campaign2"
./campaign.sh

cd "../campaign3"
./campaign.sh

cd "../campaign4"
./campaign.sh

cd "../campaign5"
./campaign.sh

cd "../campaign6"
./campaign.sh

# END THE CAMPAIGN
echo -e "\e[1;41m  \e[1;33m =============================================================== \e[0m "
echo -e "\e[1;41m  \e[1;33m ===                 STOPPING CAMPAIGNS                      === \e[0m "
echo -e "\e[1;41m  \e[1;33m =============================================================== \e[0m "
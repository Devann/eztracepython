#!/bin/bash

script_dir=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
cd "$script_dir"

echo "Cleanup :"
cd ./../../../

if [ -d "./build" ]; then
  echo "Deleting ./build/"
  rm -r "./build"
fi

if [ -d "./install" ]; then
  echo "Deleting ./install/"
  rm -r "./install"
fi

if [ -d "./python_testing/logs" ]; then
  echo "Deleting ./python_testing/logs"
  rm -r "./python_testing/logs"
fi

echo "Cleanup done"
#!/bin/bash

script_dir=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
cd "$script_dir"

./cleanup.sh

echo "Installing..."
cd ./../../../
mkdir build install
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$PWD/../install -DEZTRACE_ENABLE_PYTHON=ON -DEZTRACE_ENABLE_CUDA=ON
make install
echo -e "\n"
./../install/bin/eztrace_avail
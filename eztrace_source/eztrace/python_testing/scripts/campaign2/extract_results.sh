#!/bin/bash

# VARIABLES
TYPES=("PYTHON" "FULL" "FILTERING_PYTHON" "FILTERING_C")

# FOLDER NAMES
LOG_DIR="./../../logs"
TIMES_TRACE_DIR="$LOG_DIR/times_trace"
OTF2_TRACE_DIR="$LOG_DIR/otf2_trace"

# ARGUMENTS
if [ $# -ne 2 ]; then
    echo "Usage: $0 SCRIPT_NAME OUTPUT_FILE"
    exit 1
fi
SCRIPT_NAME=$1
OUTPUT_FILE=$2
if [ -e "$OUTPUT_FILE" ]; then
    rm "$OUTPUT_FILE"
fi

# EXTRACT RESULTS
convert_to_seconds() {
    local duration=$1
    local minutes=$(echo "$duration" | cut -dm -f1)
    local seconds=$(echo "$duration" | cut -dm -f2 | tr -d 's' | tr ',' '.')    
    local total_seconds=$(echo "$minutes * 60 + $seconds" | bc)
    if [ "${total_seconds:0:1}" = "." ]; then
        total_seconds="0$total_seconds"
    fi
    echo "$total_seconds"
}

# DISPLAY RESULTS TABLE
echo -e "\e[1;42m\e[1;33m Results for ${SCRIPT_NAME}.py \e[0m"
echo -e "\e[1;41m\e[1;33m Performance \e[0m"
echo "${SCRIPT_NAME}.py" >> "$OUTPUT_FILE"
printf "%-20s %-10s %-10s\n" "SCRIPT" "RESULT"
echo -e "SCRIPT RESULT">> "$OUTPUT_FILE"

for EZTRACEPYTHON_TESTING in "${TYPES[@]}"; do
    FILE="${TIMES_TRACE_DIR}/${EZTRACEPYTHON_TESTING}_${SCRIPT_NAME}.txt"
    RESULT=$(convert_to_seconds "$(grep "real" "$FILE" | awk '{print $2}')")
    printf "%-20s %-10s %-10s\n" "$EZTRACEPYTHON_TESTING" "${RESULT}s"
    echo "$EZTRACEPYTHON_TESTING $RESULT" >> "$OUTPUT_FILE"
done

exit 0
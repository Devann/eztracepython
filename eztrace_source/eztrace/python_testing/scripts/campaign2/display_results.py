# MODULES
import os
import sys
import pandas as pd
import matplotlib.pyplot as plt

# ARGUMENTS
if len(sys.argv) == 3:
    script_name = sys.argv[1]
    data_file = sys.argv[2]
else:
    print("Usage: display_results.py <data_file> <script_name>")
    exit()

# VÉRIFICATION DE L'EXISTENCE DU FICHIER
if not os.path.exists(data_file):
    print(f"[ERROR] In {sys.argv[0]}, the following file does not exist : {data_file}")
    exit()

# VARIABLES
CAMPAIGN_RESULTS="../../results/campaign2"
TITLE = f'[CAMPAIGN 2] Repartition of the execution time of {script_name}'
SUBTITLE=""

if script_name == "numpy_nn":
    SUBTITLE="137910 events | 24005 events not filtered (17.4%)"
if script_name == "simple_calls_many":
    SUBTITLE="8040000 events | 40000 events not filtered (0.5%)"
if script_name == "pytorch_nn":
    SUBTITLE="1103482 events | 38530 events not filtered (3.5%)"

xlabel = 'Nom du script'
ylabel = 'Temps (s)'

# FETCH DATA
df = pd.read_csv(data_file, sep='\s+', skiprows=2, names=['SCRIPT', 'RESULT'])
df_diff = df[['SCRIPT', 'RESULT']]
plt.figure(figsize=(10, 6))

# DISPLAY
COLORS = ['#D32F2F', '#FFA000', '#7B1FA2', '#8D6E63']
LABELS = ['PYTHON', 'FULL', 'FILTERING_PYTHON = FULL + FILTERING_FUNCTIONS_IN_PYTHON', 'FILTERING_C = FULL + FILTERING_FUNCTIONS_IN_C']

for i in range(len(df['SCRIPT'])):
    plt.bar(df['SCRIPT'][i], df['RESULT'][i], color=COLORS[i], label=LABELS[i])
    plt.text(df['SCRIPT'][i], df['RESULT'][i] + 0.01 * max(df['RESULT']),
             f"{df['RESULT'][i]:.3f} s", ha='center', va='bottom')

plt.grid(axis='y', linestyle='--', alpha=0.3)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.suptitle(SUBTITLE)
plt.title(TITLE, fontsize=16, fontweight='bold')
plt.ylim(0, max(df['RESULT']) + max(df['RESULT'])/2)
plt.legend(title="Scripts", loc='upper left')

# SAVE
output_file = f'{CAMPAIGN_RESULTS}/c2_result_{script_name}.png'
plt.savefig(output_file)
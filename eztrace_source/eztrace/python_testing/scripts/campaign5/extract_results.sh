#!/bin/bash

# VARIABLES
TRACING_FULL_C="TRACING_FULL_C"
TRACING_FULL_C_TIMERS="TRACING_FULL_C_TIMERS"

# FOLDER NAMES
LOG_DIR="../../logs"
TIMES_TRACE_DIR="$LOG_DIR/times_trace"

# ARGUMENTS
if [ $# -ne 2 ]; then
    echo "Usage: $0 SCRIPT_NAME OUTPUT_FILE"
    exit 1
fi
SCRIPT_NAME=$1
OUTPUT_FILE=$2

convert_to_seconds() {
    local duration=$1
    local minutes=$(echo "$duration" | cut -dm -f1)
    local seconds=$(echo "$duration" | cut -dm -f2 | tr -d 's' | tr ',' '.')    
    local total_seconds=$(echo "$minutes * 60 + $seconds" | bc)
    if [ "${total_seconds:0:1}" = "." ]; then
        total_seconds="0$total_seconds"
    fi
    echo "$total_seconds"
}

# EXTRACT RESULTS
FILE="${TIMES_TRACE_DIR}/PYTHON_${SCRIPT_NAME}.txt"
vanilla_time=$(convert_to_seconds "$(grep "real" "$FILE" | awk '{print $2}')")

FILE="${TIMES_TRACE_DIR}/${TRACING_FULL_C}_${SCRIPT_NAME}.txt"
execution_notimers_time=$(convert_to_seconds "$(grep "real" "$FILE" | awk '{print $2}')")

FILE="${TIMES_TRACE_DIR}/${TRACING_FULL_C_TIMERS}_${SCRIPT_NAME}.txt"
execution_time=$(convert_to_seconds "$(grep "real" "$FILE" | awk '{print $2}')")

## RETRIEVE C TIMERS
FILE="${TIMES_TRACE_DIR}/C_REPARTITION_${SCRIPT_NAME}.txt"
tracer=$(grep "tracer" "$FILE" | awk '{print $2}')
filtering=$(grep "filtering" "$FILE" | awk '{print $2}')
eztrace_python_enter=$(grep "eztrace_python_enter" "$FILE" | awk '{print $2}')
eztrace_python_leave=$(grep "eztrace_python_leave" "$FILE" | awk '{print $2}')
search_function_overhead=$(grep "search_function" "$FILE" | awk '{print $2}')
register_filters_overhead=$(grep "register_filters" "$FILE" | awk '{print $2}')
free_list_overhead=$(grep "free_list" "$FILE" | awk '{print $2}')

## RETRIEVE PYTHON TIMERS
c_part_total=$(grep "exec_timer" "$FILE" | awk '{print $2}')
c_part_total=$(echo "$c_part_total - $vanilla_time" | bc)

## COMPUTE USEFUL DATA
eztrace_part=$(echo "$eztrace_python_enter + $eztrace_python_leave" | bc)
full_overhead=$(echo "$execution_time - $vanilla_time" | bc)
timer_overhead=$(echo "$execution_time - $execution_notimers_time" | bc)
tracer_time=$(echo "$tracer - $eztrace_python_enter - $eztrace_python_leave - $filtering - $search_function_overhead" | bc)
total_c_timers=$(echo "$tracer + $register_filters_overhead + $free_list_overhead" | bc)
remaining_c=$(echo "$c_part_total + $tracer_time - $total_c_timers - $timer_overhead" | bc)
if [ $(echo "$remaining_c < 0" | bc) -eq 1 ]; then
    remaining_c=0
fi
python_overhead=$(echo "$execution_time - $c_part_total  - $vanilla_time" | bc)

# SAVE DATA
{
  echo -e "PART OVERHEAD"
  echo -e "Python_Execution" $vanilla_time
  echo -e "C_Filtering" $filtering
  echo -e "Python_Remaining" $python_overhead
  echo -e "Search_Function" $search_function_overhead
  echo -e "EZTrace" $eztrace_part
  echo -e "C_Part_Remaining" $remaining_c
  echo -e "Timer" $timer_overhead
} > "$OUTPUT_FILE"

exit 0
#!/bin/bash

# VARIABLES
SCRIPTS_TO_TEST=("numpy_nn" "simple_calls_many" "pytorch_nn")
TYPES=("PYTHON" "TRACING_FULL_C" "TRACING_FULL_C_TIMERS")
CAMPAIGN_NUMBER=5
DATETIME=$(date +"%Y-%m-%d_%H:%M:%S")

# FOLDER NAMES
DIR_ROOT="./../.."
DIR_LOG="$DIR_ROOT/logs"
DIR_TIME_TRACE="$DIR_LOG/times_trace"
DIR_RECAP_TRACE="$DIR_LOG/recap_trace"
DIR_RESULTS="$DIR_ROOT/results"
DIR_CAMPAIGN_RESULTS="$DIR_RESULTS/campaign${CAMPAIGN_NUMBER}"

# CLEAN INSTALL
./../tools/script_install.sh > /dev/null 2> /dev/null
mkdir -p $DIR_TIME_TRACE
mkdir -p $DIR_RECAP_TRACE
mkdir -p $DIR_RESULTS
mkdir -p $DIR_CAMPAIGN_RESULTS
rm -r "$DIR_CAMPAIGN_RESULTS"/*

# START THE CAMPAIGN
start_time=$(date +%s.%N)
echo -e "\e[1;44m  \e[1;33m =============================================================== \e[0m "
echo -e "\e[1;44m  \e[1;33m ===                 STARTING TEST CAMPAIGN                  === \e[0m "
echo -e "\e[1;44m  \e[1;33m =============================================================== \e[0m "

# FUNCTION TO LAUNCH A TEST
execute_benchmark() {
    local MODE=$1
    export EZTRACEPYTHON_TESTING=$MODE
    echo -e "Executing micro benchmark with EZTRACEPYTHON_TESTING = ${EZTRACEPYTHON_TESTING}"
    for script in "${SCRIPTS_TO_TEST[@]}"; do
        ./benchmark.sh "$script"
        echo -e "- $script measured"
    done
    echo -e "\e[1;42m\e[1;33m Done \e[0m"
}

# EXECUTE TESTS
for type in "${TYPES[@]}"; do
    execute_benchmark "$type"
done

# END THE TESTS
unset EZTRACEPYTHON_TESTING
end_time=$(date +%s.%N)
elapsed_time=$(echo "$end_time - $start_time" | bc)
echo -e "\nCampaign done in ${elapsed_time} seconds"
echo -e "You can find you results under ${LOG_DIR} \n"

# FUNCTION TO DISPLAY RESULT FOR ONE SCRIPT
display_result() {
    script_name=$1
    output_file="${DIR_RECAP_TRACE}/${script_name}_${DATETIME}.txt"
    ./extract_results.sh $script_name $output_file
    python3 display_results.py $script_name $output_file
}

# DISPLAY RESULTS
for script in "${SCRIPTS_TO_TEST[@]}"; do
    display_result $script
done

# END THE CAMPAIGN
echo -e "\e[1;44m  \e[1;33m =============================================================== \e[0m "
echo -e "\e[1;44m  \e[1;33m ===                     STOP TEST CAMPAIGN                  === \e[0m "
echo -e "\e[1;44m  \e[1;33m =============================================================== \e[0m "
#!/bin/bash

# VARIABLES
EZTRACEPYTHON_TESTING="TRACING_PYTHON_LL"

# FOLDER NAMES
LOG_DIR="../../logs"
TIMES_TRACE_DIR="$LOG_DIR/times_trace"

# ARGUMENTS
if [ $# -ne 2 ]; then
    echo "Usage: $0 SCRIPT_NAME OUTPUT_FILE"
    exit 1
fi
SCRIPT_NAME=$1
OUTPUT_FILE=$2

convert_to_seconds() {
    local duration=$1
    local minutes=$(echo "$duration" | cut -dm -f1)
    local seconds=$(echo "$duration" | cut -dm -f2 | tr -d 's' | tr ',' '.')    
    local total_seconds=$(echo "$minutes * 60 + $seconds" | bc)
    if [ "${total_seconds:0:1}" = "." ]; then
        total_seconds="0$total_seconds"
    fi
    echo "$total_seconds"
}

# EXTRACT RESULTS
FILE="${TIMES_TRACE_DIR}/PYTHON_${SCRIPT_NAME}.txt"
vanilla_time=$(convert_to_seconds "$(grep "real" "$FILE" | awk '{print $2}')")

FILE="${TIMES_TRACE_DIR}/${EZTRACEPYTHON_TESTING}_${SCRIPT_NAME}.txt"
execution_time=$(convert_to_seconds "$(grep "real" "$FILE" | awk '{print $2}')")

## RETRIEVE C TIMERS
FILE="${TIMES_TRACE_DIR}/C_REPARTITION_${SCRIPT_NAME}.txt"
eztrace_python_enter=$(grep "eztrace_python_enter" "$FILE" | awk '{print $2}')
eztrace_python_leave=$(grep "eztrace_python_leave" "$FILE" | awk '{print $2}')
search_function_overhead=$(grep "search_function" "$FILE" | awk '{print $2}')
register_filters_overhead=$(grep "register_filters" "$FILE" | awk '{print $2}')
free_list_overhead=$(grep "free_list" "$FILE" | awk '{print $2}')

## RETRIEVE PYTHON TIMERS
FILE="${TIMES_TRACE_DIR}/C_CALL_${SCRIPT_NAME}.txt"
python_conversion_overhead=$(grep "convert_timer" "$FILE" | awk '{print $2}')
c_part_overhead=$(grep "c_part" "$FILE" | awk '{print $2}')
python_filtering_overhead=$(grep "filtering_timer" "$FILE" | awk '{print $2}')

## COMPUTE USEFUL DATA
full_overhead=$(echo "$execution_time - $vanilla_time" | bc)
c_remaining=$(echo "$eztrace_python_enter + $eztrace_python_leave + $register_filters_overhead + $free_list_overhead" | bc)
python_remaining_overhead=$(echo "$full_overhead - ($c_part_overhead + $python_conversion_overhead + $python_filtering_overhead)" | bc)
clib_overhead=$(echo "$c_part_overhead - $c_remaining - $search_function_overhead" | bc)

# SAVE DATA
{
  echo -e "PART OVERHEAD"
  echo -e "Python_Execution" $vanilla_time
  echo -e "Python_Conversion" $python_conversion_overhead
  echo -e "Python_Filtering" $python_filtering_overhead
  echo -e "Python_Remaining" $python_remaining_overhead
  echo -e "Call_from_Python_to_C" $clib_overhead
  echo -e "Search_Function" $search_function_overhead
  echo -e "C_Part_Remaining" $c_remaining
} > "$OUTPUT_FILE"

exit 0
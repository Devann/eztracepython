# MODULES
import os
import sys
import pandas as pd
import matplotlib.pyplot as plt

# ARGUMENTS
if len(sys.argv) == 3:
    script_name = sys.argv[1]
    data_file = sys.argv[2]
else:
    print("Usage: display_results.py <data_file> <script_name>")
    exit()

# VARIABLES
CAMPAIGN_RESULTS="../../results/campaign3"
TITLE = f'[CAMPAIGN 3] Repartition of the execution time of {script_name}'
COLORS = ['#D32F2F', '#1976D2', '#7B1FA2', '#388E3C', '#FFA000', "#FF4081", '#8D6E63']
BAR_WIDTH=0.6

# VÉRIFICATION DE L'EXISTENCE DU FICHIER
if not os.path.exists(data_file):
    print(f"[ERROR] In {sys.argv[0]}, the following file does not exist : {data_file}")
    exit()

# DISPLAY DATA
dataframe = pd.read_csv(data_file, sep='\s+', skiprows=1, names=['PART', 'OVERHEAD'])
plt.figure(figsize=(10, 8))
plt.bar("PYTHON", dataframe['OVERHEAD'][0], color='#D32F2F', width=BAR_WIDTH)

bottom = 0
BAR_WITH_LEGEND=[]
for i in range(0, len(dataframe['OVERHEAD'])):
    BAR_LABEL=dataframe['PART'][i] + ': ' + round(dataframe['OVERHEAD'][i], 3).astype(str)
    BAR = plt.bar("FULL", dataframe['OVERHEAD'][i], bottom=bottom, color=COLORS[i], label=BAR_LABEL, width=BAR_WIDTH)
    BAR_WITH_LEGEND.append(BAR)
    bottom += dataframe['OVERHEAD'][i]

# DISPLAY META INFORMATION
plt.title(TITLE, fontsize=16, fontweight='bold')
plt.legend(handles=BAR_WITH_LEGEND, title="Duration (in seconds)", loc='upper left')

if script_name == "numpy_nn":
    plt.ylim(0, 2.3)
if script_name == "simple_calls_many":
    plt.ylim(0, 40)
if script_name == "pytorch_nn":
    plt.ylim(0, 60)

# SAVE
output_file = f'{CAMPAIGN_RESULTS}/c3_result_{script_name}.png'
plt.savefig(output_file)
#!/bin/bash
PYTHON_TO_EXECUTE="pytorch_nn"
ARGS=""

script_dir=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
cd "$script_dir"

./../tools/script_install.sh

export EZTRACEPYTHON_TESTING="DEBUG"

./../../../install/bin/eztrace.preload -t python -o ./../../logs/otf2-trace/${EZTRACEPYTHON_TESTING}_${PYTHON_TO_EXECUTE} "./../../python_files/${PYTHON_TO_EXECUTE}.py" "${ARGS}";

#!/bin/bash

EZTRACE_PATH=./../../../
cd $EZTRACE_PATH
echo $PWD

./python_testing/scripts/tools/script_install.sh

export EZTRACEPYTHON_TESTING=TRACING_FULL_C
# echo "linspace sin tensor unsqueeze print zero_grad backward parameters" > ./python_testing/scripts/scripts_perso/filter.txt
# export EZTPY_FUNCTION_FILTER="./python_testing/scripts/scripts_perso/filter.txt"
# export EZTPY_CALLER_FILTER=1

start_time=$(date +%s.%N)
./install/bin/eztrace.preload -t "python cuda" -o ./python_testing/logs/otf2-trace/FILTERING_pytorch ./python_testing/python_files/pytorch_nn.py
end_time=$(date +%s.%N)
duration=$(echo "$end_time - $start_time" | bc)

echo "Script execution time: $duration seconds"

otf2-print ./python_testing/logs/otf2-trace/FILTERING_pytorch/eztrace_log.otf2 > ./python_testing/scripts/scripts_perso/results_c.txt 2> /dev/null
vite ./python_testing/logs/otf2-trace/FILTERING_pytorch/eztrace_log.otf2
#!/bin/bash
NB_EXEC=20000
NB_EXEC_INSIDE=200
NB_MENTIONS_A=$((NB_EXEC * 2))
NB_MENTIONS_B=$((NB_EXEC * NB_EXEC_INSIDE * 2))
TOTAL_MENTIONS=$((NB_MENTIONS_A + NB_MENTIONS_B + 2))

export EZTRACEPYTHON_TESTING=FILTERING_C
script_dir=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
cd "$script_dir"

echo -e "\e[1;44m  \e[1;33m =============================================================== \e[0m "
echo -e "\e[1;44m  \e[1;33m ===                      INSTALLING                         === \e[0m "
echo -e "\e[1;44m  \e[1;33m =============================================================== \e[0m "

./../tools/script_install.sh

echo -e "\e[1;44m  \e[1;33m =============================================================== \e[0m "
echo -e "\e[1;44m  \e[1;33m ===               TESTING simple_calls_many                 === \e[0m "
echo -e "\e[1;44m  \e[1;33m =============================================================== \e[0m "

test_number=4
test_passed=0
export EZTPY_FUNCTION_FILTER="function_filter.txt"
echo "Testing filtering of function a()"
echo "a" > "function_filter.txt"
./../../../install/bin/eztrace.preload -t python -o ./../../logs/otf2-trace/FILTERING_a_simple_calls_many "./../../python_files/simple_calls_many.py";
otf2-print ./../../logs/otf2-trace/FILTERING_a_simple_calls_many/eztrace_log.otf2 > ./../../logs/otf2-trace/FILTERING_a_simple_calls_many/otf2-print-results.txt
count=$(awk '/^(ENTER|LEAVE)/ && /Region: "a"/ { count++ } END { print count }' ./../../logs/otf2-trace/FILTERING_a_simple_calls_many/otf2-print-results.txt)
if [[ $count == $NB_MENTIONS_A ]]; then
    echo "SUCCES : Function a correctly logged ${count} times"
    ((test_passed++))
else
    echo "FAILURE : Function a counted ${count} times, should be ${NB_MENTIONS_A}"
fi
echo -e "\n"

echo "Testing filtering of function b()"
echo b > "./function_filter.txt"
./../../../install/bin/eztrace.preload -t python -o ./../../logs/otf2-trace/FILTERING_b_simple_calls_many "./../../python_files/simple_calls_many.py";
otf2-print ./../../logs/otf2-trace/FILTERING_b_simple_calls_many/eztrace_log.otf2 > ./../../logs/otf2-trace/FILTERING_b_simple_calls_many/otf2-print-results.txt
count=$(awk '/^(ENTER|LEAVE)/ && /Region: "b"/ { count++ } END { print count }' ./../../logs/otf2-trace/FILTERING_b_simple_calls_many/otf2-print-results.txt)
if [[ $count == $NB_MENTIONS_B ]]; then
    echo "SUCCES : Function b correctly logged ${count} times"
    ((test_passed++))
else
    echo "FAILURE : Function b counted ${count} times, should be ${NB_MENTIONS_B}"
fi
echo -e "\n"
unset EZTPY_FUNCTION_FILTER

echo "Testing filtering of all functions only from the source file"
echo a > "./function_filter.txt"
export EZTPY_SOURCE_FILTER=1
./../../../install/bin/eztrace.preload -t python -o ./../../logs/otf2-trace/FILTERING_sourceonly_simple_calls_many "./../../python_files/simple_calls_many.py";
otf2-print ./../../logs/otf2-trace/FILTERING_sourceonly_simple_calls_many/eztrace_log.otf2 > ./../../logs/otf2-trace/FILTERING_sourceonly_simple_calls_many/otf2-print-results.txt
count=$(awk '/^(ENTER|LEAVE)/ { count++ } END { print count }' ./../../logs/otf2-trace/FILTERING_sourceonly_simple_calls_many/otf2-print-results.txt)
if [[ $count == $TOTAL_MENTIONS ]]; then
    echo "SUCCES : Source functions correctly logged ${count} times"
    ((test_passed++))
else
    echo "FAILURE : Source functions counted ${count} times, should be ${TOTAL_MENTIONS}"
fi
echo -e "\n"

echo "Testing filtering of function a() + only from the source file"
export EZTPY_FUNCTION_FILTER="function_filter.txt"
./../../../install/bin/eztrace.preload -t python -o ./../../logs/otf2-trace/FILTERING_both_simple_calls_many "./../../python_files/simple_calls_many.py" -pyf "function_filter.txt" -pys;
otf2-print ./../../logs/otf2-trace/FILTERING_both_simple_calls_many/eztrace_log.otf2 > ./../../logs/otf2-trace/FILTERING_both_simple_calls_many/otf2-print-results.txt
count=$(awk '/^(ENTER|LEAVE)/ { count++ } END { print count }' ./../../logs/otf2-trace/FILTERING_both_simple_calls_many/otf2-print-results.txt)
if [[ $count == $NB_MENTIONS_A ]]; then
    echo "SUCCES : Functions correctly logged ${count} times"
    ((test_passed++))
else
    echo "FAILURE : Functions counted ${count} times, should be ${NB_MENTIONS_A}"
fi
echo -e "\n"
echo -e "Test Passed : $test_passed / $test_number \n"
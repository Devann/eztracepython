#!/bin/bash

EZTRACE_PATH=/home/jules/TSP/CSC_PFE/eztracepython/eztrace_source/eztrace
cd $EZTRACE_PATH

./python_testing/scripts/tools/script_install.sh
sleep 2

# Python only
start_time=$(date +%s.%N)
python3 ./python_testing/python_files/pytorch_nn.py
end_time=$(date +%s.%N)
python_duration=$(echo "$end_time - $start_time" | bc)

# Filter setup
echo "linspace sin tensor unsqueeze print zero_grad backward parameters" > ./python_testing/scripts/scripts_perso/filter.txt
export EZTPY_FUNCTION_FILTER="./python_testing/scripts/scripts_perso/filter.txt"

# C tracer with timers and with default compilation
export EZTRACEPYTHON_TESTING=TRACING_FULL_C
start_time=$(date +%s.%N)
./install/bin/eztrace.preload -t python -o ./python_testing/logs/otf2-trace/pytorch ./python_testing/python_files/pytorch_nn.py
end_time=$(date +%s.%N)
tracing_c_duration=$(echo "$end_time - $start_time" | bc)

# C tracer without timers and with release compilation
./python_testing/scripts/tools/script_install_perf.sh
sleep 2
export EZTRACEPYTHON_TESTING=TRACING_FULL_C
start_time=$(date +%s.%N)
./install/bin/eztrace.preload -t python -o ./python_testing/logs/otf2-trace/pytorch ./python_testing/python_files/pytorch_nn.py
end_time=$(date +%s.%N)
tracing_c_perf_duration=$(echo "$end_time - $start_time" | bc)

echo "Scripts execution time :"
echo "Python = ${python_duration} s"
echo "Tracer C = ${tracing_c_duration} s"
echo "Tracer C Perf = ${tracing_c_perf_duration} s"

otf2-print ./python_testing/logs/otf2-trace/pytorch/eztrace_log.otf2 > ./python_testing/scripts/scripts_perso/results_c.txt 2> /dev/null
vite ./python_testing/logs/otf2-trace/pytorch/eztrace_log.otf2
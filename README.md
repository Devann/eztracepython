# EzTracePython

EzTracePython is a plugin for the tool [EzTrace](https://eztrace.gitlab.io/eztrace/). This teamwork project is carried out as part of the third-year curriculum at Télécom SudParis, a French engineering school. It is supervised by François Trahay, researcher and professor at Télécom SudParis.

## Team Members

- Devan PRIGENT (devan.prgt@gmail.com)
- Jules RISSES (jules.risse@gmail.com)
- Meryem BOUHARRA (meryem.bouharra@telecom-sudparis.eu)
